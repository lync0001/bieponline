{{-- Each menu item is a <li> with an <a> inside --}}
<li>
	<a class="nav-container" href="{!! url('user') !!}">
		<i class="fa fa-users fa-2x fa-fw"></i>&nbspGebruikers
	</a>
</li>
<li>
	<a class="nav-container" href="{!! url('role') !!}">
		<i class="fa fa-map fa-2x fa-fw"></i>&nbspRollen
	</a>
</li>

